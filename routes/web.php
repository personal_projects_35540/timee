<?php

/**
 * File: routes/web.php
 *
 * File containing web routes.
 *
 * @package   web_routes
 * @category  Routes
 * @author    Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @copyright 2021 Pastrikos Konstantinos
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version   0.1.0
 * @since     File available since Release 0.0.1
 */

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\RecordController;
use App\Http\Controllers\TrackingController;

/**
 * List of registered routes until version 0.1.0.
 *
 *+--------+-----------+----------------------------+--------------------+-------------------------------------------------+------------+
 *| Domain | Method    | URI                        | Name               | Action                                          | Middleware |
 *+--------+-----------+----------------------------+--------------------+-------------------------------------------------+------------+
 *|        | GET|HEAD  | /                          |                    | Closure                                         | web        |
 *|        | GET|HEAD  | api/user                   |                    | Closure                                         | api        |
 *|        |           |                            |                    |                                                 | auth:api   |
 *|        | GET|HEAD  | categories                 | categories.index   | App\Http\Controllers\CategoryController@index   | web        |
 *|        | POST      | categories                 | categories.store   | App\Http\Controllers\CategoryController@store   | web        |
 *|        | GET|HEAD  | categories/create          | categories.create  | App\Http\Controllers\CategoryController@create  | web        |
 *|        | GET|HEAD  | categories/{category}      | categories.show    | App\Http\Controllers\CategoryController@show    | web        |
 *|        | PUT|PATCH | categories/{category}      | categories.update  | App\Http\Controllers\CategoryController@update  | web        |
 *|        | DELETE    | categories/{category}      | categories.destroy | App\Http\Controllers\CategoryController@destroy | web        |
 *|        | GET|HEAD  | categories/{category}/edit | categories.edit    | App\Http\Controllers\CategoryController@edit    | web        |
 *|        | GET|HEAD  | records                    | records.index      | App\Http\Controllers\RecordController@index     | web        |
 *|        | POST      | records                    | records.store      | App\Http\Controllers\RecordController@store     | web        |
 *|        | GET|HEAD  | records/create             | records.create     | App\Http\Controllers\RecordController@create    | web        |
 *|        | GET|HEAD  | records/{record}           | records.show       | App\Http\Controllers\RecordController@show      | web        |
 *|        | PUT|PATCH | records/{record}           | records.update     | App\Http\Controllers\RecordController@update    | web        |
 *|        | DELETE    | records/{record}           | records.destroy    | App\Http\Controllers\RecordController@destroy   | web        |
 *|        | GET|HEAD  | records/{record}/edit      | records.edit       | App\Http\Controllers\RecordController@edit      | web        |
 *|        | GET|HEAD  | tasks                      | tasks.index        | App\Http\Controllers\TaskController@index       | web        |
 *|        | POST      | tasks                      | tasks.store        | App\Http\Controllers\TaskController@store       | web        |
 *|        | GET|HEAD  | tasks/create               | tasks.create       | App\Http\Controllers\TaskController@create      | web        |
 *|        | GET|HEAD  | tasks/{task}               | tasks.show         | App\Http\Controllers\TaskController@show        | web        |
 *|        | PUT|PATCH | tasks/{task}               | tasks.update       | App\Http\Controllers\TaskController@update      | web        |
 *|        | DELETE    | tasks/{task}               | tasks.destroy      | App\Http\Controllers\TaskController@destroy     | web        |
 *|        | GET|HEAD  | tasks/{task}/edit          | tasks.edit         | App\Http\Controllers\TaskController@edit        | web        |
 *+--------+-----------+----------------------------+--------------------+-------------------------------------------------+------------+
 */

Route::redirect('/', '/tracking');

Route::get('/tracking', [TrackingController::class, 'index']);


/**
 * Custom routes registration.
 * 
 * @since 0.1.0 Registered routes for categories, tasks and records.
 */
Route::resource('categories', CategoryController::class);
Route::resource('tasks', TaskController::class);
Route::resource('records', RecordController::class);
