<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <title>Timee Time Tracker</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/fontawesome-iconpicker.min.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900|Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&amp;display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/fonts/css/fontawesome-all.min.css">
    <link rel="manifest" href="_manifest.json" data-pwa-version="set_in_manifest_and_pwa_js">
    <link rel="apple-touch-icon" sizes="180x180" href="app/icons/icon-192x192.png">
</head>

<body class="theme-light">

    <div id="preloader">
        <div class="spinner-border color-highlight" role="status"></div>
    </div>

    <div id="page">

        <!-- header and footer bar go here-->
        <div class="header header-fixed header-auto-show header-logo-app">
            <a href="/tracking" data-back-button class="header-title header-subtitle">Back to Tracking</a>
            <a href="#" data-back-button class="header-icon header-icon-1"><i class="fas fa-arrow-left"></i></a>
            <a href="#" data-toggle-theme class="header-icon header-icon-2 show-on-theme-dark"><i class="fas fa-sun"></i></a>
            <a href="#" data-toggle-theme class="header-icon header-icon-2 show-on-theme-light"><i class="fas fa-moon"></i></a>
            <a href="#" data-menu="menu-highlights" class="header-icon header-icon-3"><i class="fas fa-brush"></i></a>
            <a href="#" data-menu="menu-main" class="header-icon header-icon-4"><i class="fas fa-bars"></i></a>
        </div>
        <div id="footer-bar" class="footer-bar-5">
            <a href="/tracking" @if (request()->is('tracking')) class="active-nav" @endif>
                <i data-feather="clock" data-feather-line="1" data-feather-size="21" data-feather-color="green-dark" data-feather-bg="green-fade-light"></i><span>Tracking</span></a>
            <a href="/categories" @if (request()->is('categories')) class="active-nav" @endif>
                <i data-feather="tag" data-feather-line="1" data-feather-size="21" data-feather-color="red-dark" data-feather-bg="red-fade-light"></i><span>Categories</span></a>
            <a href="/tasks" @if (request()->is('tasks')) class="active-nav" @endif>
                <i data-feather="check-circle" data-feather-line="1" data-feather-size="21" data-feather-color="blue-dark" data-feather-bg="blue-fade-light"></i><span>Tasks</span></a>
            <a href="/calendar" @if (request()->is('calendar')) class="active-nav" @endif>
                <i data-feather="calendar" data-feather-line="1" data-feather-size="21" data-feather-color="brown-dark" data-feather-bg="brown-fade-light"></i><span>Calendar</span></a>
            <a href="/settings" @if (request()->is('settings')) class="active-nav" @endif>
                <i data-feather="settings" data-feather-line="1" data-feather-size="21" data-feather-color="dark-dark" data-feather-bg="gray-fade-light"></i><span>Settings</span></a>
        </div>



        @if (request()->is('tracking') || request()->is('/'))
        @include('tracking')
        @endif

        @if (request()->is('tasks'))
        @include('tasks')

        @elseif (Request::is('tasks/create'))
        @include('task-create')

        @elseif (Request::is('tasks/*'))
        @include('task')
        @endif


        @if (request()->is('categories'))
        @include('categories')

        @elseif (Request::is('categories/create'))
        @include('category-create')

        @elseif (Request::is('categories/*'))
        @include('category')
        @endif

        <!-- footer and footer card-->
        <!-- <div class="footer" data-menu-load="menu-footer.html"></div> -->




        <!-- <div id="menu-share" class="menu menu-box-bottom menu-box-detached rounded-m" data-menu-load="menu-share.html" data-menu-height="420" data-menu-effect="menu-over">
        </div>

        <div id="menu-highlights" class="menu menu-box-bottom menu-box-detached rounded-m" data-menu-load="menu-colors.html" data-menu-height="510" data-menu-effect="menu-over">
        </div>

        <div id="menu-main" class="menu menu-box-right menu-box-detached rounded-m" data-menu-width="260" data-menu-load="menu-main.html" data-menu-active="nav-pages" data-menu-effect="menu-over">
        </div> -->


    </div>

    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"> </script>

    <script src="{{ asset('js/easytimer.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>



</body>

</html>