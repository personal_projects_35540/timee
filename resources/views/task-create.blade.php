<div class="page-content">
    <div class="page-title page-title-small">
        <h2><a href="/tasks" data-back-button><i class="fa fa-arrow-left"></i></a>Tasks</h2>
        <a href="#" data-menu="menu-main" class="bg-fade-highlight-light shadow-xl preload-img" data-src="/images/avatars/5s.png"></a>
    </div>
    <div class="card header-card shape-rounded" data-card-height="150">
        <div class="card-overlay bg-highlight opacity-95"></div>
        <div class="card-overlay dark-mode-tint"></div>
        <div class="card-bg preload-img" data-src="/images/pictures/20s.jpg"></div>
    </div>

    <div class="card card-style">
        <div class="content mb-0">

            <form method="POST" action="{{ route('tasks.store') }}">
                <!-- Since HTML forms can't make PUT, PATCH, or DELETE requests, you will need to add a hidden _method field to spoof these HTTP verbs.-->
                <!-- The method Blade directive can create this field for you -->
                @method('POST')

                <!-- CROSS Site Request Forgery Protection -->
                @csrf

                <h3 class="font-600">Create Task</h3>
                <p>
                    Add task information.
                </p>

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <div class="input-style has-borders hnoas-icon input-style-always-active validate-field mb-4">
                    <input type="name" class="form-control validate-name" id="form1" name="label" placeholder="Set task label">
                    <label for="form1" class="color-highlight font-400 font-13">Label</label>
                    <i class="fa fa-times disabled invalid color-red-dark"></i>
                    <i class="fa fa-check disabled valid color-green-dark"></i>
                    <em>(required)</em>
                </div>

                <div class="input-style has-borders no-icon input-style-always-active validate-field mb-4">
                    <input type="description" class="form-control" id="form2" name="description" placeholder="Set task description">
                    <label for="form2" class="color-highlight font-400 font-13">Description</label>
                    <i class="fa fa-times disabled invalid color-red-dark"></i>
                    <i class="fa fa-check disabled valid color-green-dark"></i>
                    <em>(optional)</em>
                </div>

                <div class="input-style has-borders no-icon input-style-always-active validate-field mb-4">

                    <input type="icon" class="form-control icon-picker" id="form3" name="category" placeholder="Select a category from the list">
                    <label for="form3" class="color-highlight font-400 font-13">Category</label>
                    <i class="fa fa-times disabled invalid color-red-dark"></i>
                    <i class="fa fa-check disabled valid color-green-dark"></i>
                    <em>(required)</em>
                </div>

                <div class="input-style has-borders no-icon input-style-always-active validate-field mb-4">

                    <input type="icon" class="form-control icon-picker" id="form3" name="icon" placeholder="Select an icon from the list">
                    <label for="form3" class="color-highlight font-400 font-13">Icon</label>
                    <i class="fa fa-times disabled invalid color-red-dark"></i>
                    <i class="fa fa-check disabled valid color-green-dark"></i>
                    <em>(required)</em>
                </div>

                <div class="input-style has-borders no-icon input-style-always-active validate-field mb-4">
                    <input type="color" class="form-control" id="form44" name="color" placeholder="Select a color with the picker">
                    <label for="form44" class="color-highlight font-400 font-13">Color</label>
                    <i class="fa fa-times disabled invalid color-red-dark"></i>
                    <i class="fa fa-check disabled valid color-green-dark"></i>
                    <em>(required)</em>
                </div>


                <div class="d-flex no-effect collapsed mb-1" data-trigger-switch="toggle-id-4" data-bs-toggle="collapse" href="#collapseExample4" role="button" aria-expanded="false" aria-controls="collapseExample4">
                    <div class="pt-1">
                        <h5 class="font-600 font-14">Set as repetitive</h5>
                    </div>
                    <div class="ms-auto me-4 pe-2">
                        <div class="custom-control android-switch">
                            <input type="checkbox" class="android-input" id="toggle-id-4" name="repetitive">
                            <label class="custom-control-label" for="toggle-id-4"></label>
                        </div>
                    </div>
                </div>

                <div class="divider divider-margins"></div>

                <div class="row mb-1 mt-2">
                    <div class="col-6 pe-1">
                        <a href="/tasks" data-menu="menu-confirm" class="btn btn-m btn-full mb-3 rounded-xs text-uppercase font-900 shadow-s bg-red-light">Cancel</a>


                    </div>
                    <div class="col-6">
                        <button type="submit" class="btn btn-m btn-full mb-3 rounded-xs text-uppercase font-900 shadow-s bg-blue-dark">Save</button>
                    </div>
                </div>

            </form>

        </div>
    </div>

</div>
<!-- end of page content-->