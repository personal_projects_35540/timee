<div class="page-content">
    <div class="page-title page-title-small">
        <h2><a href="/categories" data-back-button><i class="fa fa-arrow-left"></i></a>Categories</h2>
        <a href="#" data-menu="menu-main" class="bg-fade-highlight-light shadow-xl preload-img" data-src="/images/avatars/5s.png"></a>
    </div>
    <div class="card header-card shape-rounded" data-card-height="150">
        <div class="card-overlay bg-highlight opacity-95"></div>
        <div class="card-overlay dark-mode-tint"></div>
        <div class="card-bg preload-img" data-src="/images/pictures/20s.jpg"></div>
    </div>

    <div class="card card-style">
        <div class="content mb-0">

            <form method="POST" action="{{ route('categories.update', $category->id) }}">
                <!-- Since HTML forms can't make PUT, PATCH, or DELETE requests, you will need to add a hidden _method field to spoof these HTTP verbs.-->
                <!-- The method Blade directive can create this field for you -->
                @method('PUT')


                <!-- CROSS Site Request Forgery Protection -->
                @csrf


                <h3 class="font-600">Edit Category</h3>
                <p>
                    Edit category information.
                </p>

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif


                <div class="input-style has-borders hnoas-icon input-style-always-active validate-field mb-4">
                    <input type="name" class="form-control validate-name" id="form1" name="label" placeholder="Set category label" value="{{$category->label}}">
                    <label for="form1" class="color-highlight font-400 font-13">Label</label>
                    <i class="fa fa-times disabled invalid color-red-dark"></i>
                    <i class="fa fa-check disabled valid color-green-dark"></i>
                    <em>(required)</em>
                </div>

                <div class="input-style has-borders no-icon input-style-always-active validate-field mb-4">
                    <input type="description" class="form-control" id="form2" name="description" placeholder="Set category description" value="{{$category->description}}">
                    <label for="form2" class="color-highlight font-400 font-13">Description</label>
                    <i class="fa fa-times disabled invalid color-red-dark"></i>
                    <i class="fa fa-check disabled valid color-green-dark"></i>
                    <em>(optional)</em>
                </div>

                <div class="input-style has-borders no-icon input-style-always-active validate-field mb-4">




                    <input type="icon" class="form-control icon-picker" id="form3" name="icon" placeholder="Select an icon from the list" value="{{$category->icon}}">
                    <label for="form3" class="color-highlight font-400 font-13">Icon</label>
                    <i class="fa fa-times disabled invalid color-red-dark"></i>
                    <i class="fa fa-check disabled valid color-green-dark"></i>
                    <em>(required)</em>
                </div>

                <div class="input-style has-borders no-icon input-style-always-active validate-field mb-4">
                    <input type="color" class="form-control" id="form44" name="color" placeholder="Select a color with the picker" value="{{$category->color}}">
                    <label for="form44" class="color-highlight font-400 font-13">Color</label>
                    <i class="fa fa-times disabled invalid color-red-dark"></i>
                    <i class="fa fa-check disabled valid color-green-dark"></i>
                    <em>(required)</em>
                </div>


                <div class="divider divider-margins"></div>

                <div class="row mb-1 mt-2">
                    <div class="col-6 pe-1">
                        <a href="#" data-menu="menu-confirm" class="deleteCategoryBtn btn btn-m btn-full mb-3 rounded-xs text-uppercase font-900 shadow-s bg-red-light">Delete</a>


                    </div>
                    <div class="col-6">
                        <button type="submit" class="btn btn-m btn-full mb-3 rounded-xs text-uppercase font-900 shadow-s bg-blue-dark">Save</button>
                    </div>
                </div>

            </form>

        </div>
    </div>

</div>
<!-- end of page content-->



<!---------------->
<!---------------->
<!--Menu Confirm-->
<!---------------->
<!---------------->

<form method="POST" action="{{ route('categories.destroy', $category->id) }}">
    <!-- Since HTML forms can't make PUT, PATCH, or DELETE requests, you will need to add a hidden _method field to spoof these HTTP verbs.-->
    <!-- The method Blade directive can create this field for you -->
    @method('DELETE')
    <!-- CROSS Site Request Forgery Protection -->
    @csrf
    <div id="menu-confirm" class="menu menu-box-bottom menu-box-detached rounded-m" data-menu-height="180" data-menu-effect="menu-over">
        <h2 class="text-center font-700 mt-3 pt-1">Are you sure?</h2>
        <p class="boxed-text-l">
            This action is irreversible.
        </p>
        <div class="row me-3 ms-4">

            <div class="col-6">
                <button type="button" class="close-menu btn btn-xl btn-full button-s shadow-l rounded-s text-uppercase font-900 bg-red-dark">Reject</button>
            </div>

            <div class="col-6">
                <button type="submit" class="close-menu btn btn-xl btn-full button-s shadow-l rounded-s text-uppercase font-900 bg-green-dark">Accept</button>
            </div>
        </div>
    </div>
</form>