<div class="page-content">
    <div class="page-title page-title-small">
        <h2><a href="/tracking" data-back-button><i class="fa fa-tasks"></i></a>Tasks</h2>
        <a href="#" data-menu="menu-main" class="bg-fade-highlight-light shadow-xl preload-img" data-src="images/avatars/5s.png"></a>
        <a href="/tasks/create" data-menu="menu-main" class="bg-fade-highlight-light shadow-xl preload-img" style="right:56px">
            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-plus-circle fa-w-16 fa-3x">
                <path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm144 276c0 6.6-5.4 12-12 12h-92v92c0 6.6-5.4 12-12 12h-56c-6.6 0-12-5.4-12-12v-92h-92c-6.6 0-12-5.4-12-12v-56c0-6.6 5.4-12 12-12h92v-92c0-6.6 5.4-12 12-12h56c6.6 0 12 5.4 12 12v92h92c6.6 0 12 5.4 12 12v56z" class=""></path>
            </svg>
        </a>

    </div>
    <div class="card header-card shape-rounded" data-card-height="150">
        <div class="card-overlay bg-highlight opacity-95"></div>
        <div class="card-overlay dark-mode-tint"></div>
        <div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
    </div>

    <!-- <div class="card card-style">
    <div class="content">
        Include useful information under each ToDo item as a small subtitle or side note for this todo.
    </div>
</div> -->

    <div class="card card-style">

        @include('flash-message')
        <!-- <div class="card header-card" data-card-height="70">
        <div class="content">
            <div class="d-flex">
                <div>
                    <h1 class="mb-0 ms-1 pt-">Tasks</h1>
                    <p class="color-highlight font-11 mt-n2 mb-3 ms-1">Here you can manage your tasks.</p>
                </div>
            </div>
        </div>
    </div>
    <div style="height:70px;"></div> -->
        <div class="content">
            <div class="todo-list list-group list-custom-large">




                @foreach ($tasks as $task)

                <a href="/tasks/{{$task->id}}/edit">
                    <i class="fa fa-{{$task->icon ?? tag}} rounded-xl bg-fade-blue-dark font-12" style="background-color:{{$task->color ?? color-blue-dark}} !important"></i>
                    <span>{{ \Illuminate\Support\Str::limit($task->label, 30, '...') }}</span>
                    <strong>{{ \Illuminate\Support\Str::limit($task->description, 40, '...') }}</strong>

                    <i class="fa fa-angle-right"></i>
                </a>



                @endforeach

            </div>
        </div>
    </div>
</div>
<!-- end of page content-->