<div class="page-content">
    <div class="page-title page-title-large">
        <h2 data-username="Kostas" class="greeting-text"></h2>
        <a href="#" data-menu="menu-main" class="bg-fade-highlight-light shadow-xl preload-img" data-src="images/avatars/5s.png"></a>
    </div>
    <div class="card header-card shape-rounded" data-card-height="180">
        <div class="card-overlay bg-highlight opacity-95"></div>
        <div class="card-overlay dark-mode-tint"></div>
        <div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
    </div>

    @foreach ($tasks as $task)

    <a href="#" class="card card-style mb-3 d-flex" data-card-height="100">
        <div class="d-flex justify-content-between">
            <div class="ps-2 ms-1 align-self-center">
                <h5 class="font-600 mb-0 pt-4 mt-1"> {{ \Illuminate\Support\Str::limit($task->label, 50, '...') }}</h5>
                <p class="color-highlight mt-n1 font-11">
                    {{ \Illuminate\Support\Str::limit($task->category->label, 40, '...') }}
                </p>
            </div>
            <div class="ps-2 ms-1 align-self-center">
                <h5 class="font-600 mb-0 pt-4 mt-1">
                    <div id="basicUsage">00:00:00</div>
                </h5>
                <!-- <p class="color-highlight mt-n1 font-11">
                02:12 Today
            </p> -->

            </div>
            <div class="pe-2 align-self-center">
                <i class="play-btn" data-task-id="{{$task->id}}" data-feather="play" data-feather-line="1" data-feather-size="45" data-feather-color="green-dark" data-feather-bg="green-fade-light"></i>
                <i class="pause-btn" data-task-id="{{$task->id}}" data-feather="pause" data-feather-line="1" data-feather-size="45" data-feather-color="yellow-dark" data-feather-bg="yellow-fade-light" style="display:none;"></i>

            </div>
        </div>
    </a>

    @endforeach


</div>
<!-- end of page content-->