<?php

/**
 * File: database/migrations/2021_06_17_000001_create_tasks_table.php
 *
 * Migration file for creating tasks table.
 *
 * @package   tasks_table_migration
 * @category  Migrations
 * @author    Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @copyright 2021 Pastrikos Konstantinos
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version   0.0.2
 * @since     File available since Release 0.0.1
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/** 
 * Migration class for creating tasks table.
 * 
 * @category Migrations
 * @author   Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @access   public 
 * @see      Migration
 */
class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * The up method is used to add tasks table and label index to the database.
     * 
     * Columns Definition.
     * label        The label of the task. Must be unique. Used as index.
     * description  Short description.
     * category_id  The if of the category that the task belongs to.
     * icon         User defined icon.
     * color        User defined color. Used in calendar.
     * completed    Boolean true or false.
     * repetitive   Specifies whether the job is repetitive, ie whether it will be displayed daily.
     *
     * @return void
     * 
     * @since 0.0.1
     * @since 0.0.2 Set description as nullable.
     */
    public function up()
    {
        Schema::create(
            'tasks',
            function (Blueprint $table) {
                $table->id();
                $table->string('label')->unique();
                $table->string('description')->nullable();
                $table->unsignedBigInteger('category_id');
                $table->char('icon', 50);
                $table->char('color', 50);
                $table->boolean('completed');
                $table->boolean('repetitive');
                $table->timestamps();
                $table->softDeletes();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * 
     * @since 0.0.1
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
