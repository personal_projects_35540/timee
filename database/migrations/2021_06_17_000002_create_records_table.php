<?php
/**
 * File: database/migrations/2021_06_17_000002_create_records_table.php
 *
 * Migration file for creating records table.
 *
 * @package   records_table_migration
 * @category  Migrations
 * @author    Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @copyright 2021 Pastrikos Konstantinos
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version   0.0.1
 * @since     File available since Release 0.0.1
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/** 
 * Migration class for creating records table.
 * 
 * @category Migrations
 * @author   Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @access   public 
 * @see      Migration
 */ 
class CreateRecordsTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * The up method is used to add records table to the database.
     * Each record should belong to one task and have a specific duration (started_at,ended_at).
     * 
     * Columns Definition.
     * task_id     The if of the task that the record belongs to.
     * started_at  Datetime of start time.
     * ended_at    Datetime of end time.
     *
     * @return void
     * 
     * @since 0.0.1
     */
    public function up()
    {
        Schema::create(
            'records', function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('task_id');
                $table->dateTime('started_at');
                $table->dateTime('ended_at');
                $table->timestamps();
                $table->softDeletes();

            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * 
     * @since 0.0.1
     */
    public function down()
    {
        Schema::dropIfExists('records');
    }
}
