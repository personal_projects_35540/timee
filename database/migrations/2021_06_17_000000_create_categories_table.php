<?php

/**
 * File: database/migrations/2021_06_17_000000_create_categories_table.php
 *
 * Migration file for creating categories table.
 *
 * @package   categories_table_migration
 * @category  Migrations
 * @author    Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @copyright 2021 Pastrikos Konstantinos
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version   0.0.2
 * @since     File available since Release 0.0.1
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/** 
 * Migration class for creating categories table.
 * 
 * @category Migrations
 * @author   Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @access   public 
 * @see      Migration
 */
class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * The up method is used to add categories table and label index to the database.
     * 
     * Columns Definition.
     * label        The label of the category. Must be unique. Used as index.
     * description  Short description.
     * icon         User defined icon.
     * color        User defined color. Used in charts.
     *
     * @return void
     * 
     * @since 0.0.1
     * @since 0.0.2 Set description as nullable.
     */
    public function up()
    {
        Schema::create(
            'categories',
            function (Blueprint $table) {
                $table->id();
                $table->string('label')->unique();
                $table->string('description')->nullable();
                $table->char('icon', 50);
                $table->char('color', 50);
                $table->timestamps();
                $table->softDeletes();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
