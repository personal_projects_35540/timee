<?php

/**
 * File: database/seeders/UserSeeder.php
 *
 * Seeder file for User records.
 *
 * @package   user_seeder
 * @category  Seeders
 * @author    Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @copyright 2021 Pastrikos Konstantinos
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version   0.1.0
 * @since     File available since Release 0.0.1
 */

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * 
     * This methos creates 10 users.
     *
     * @return void
     */
    public function run()
    {
        User::factory(10)->create();
    }
}
