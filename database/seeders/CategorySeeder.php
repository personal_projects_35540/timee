<?php

/**
 * File: database/seeders/CategorySeeder.php
 *
 * Seeder file for Category records.
 *
 * @package   categories_seeder
 * @category  Seeders
 * @author    Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @copyright 2021 Pastrikos Konstantinos
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version   0.1.0
 * @since     File available since Release 0.0.1
 */

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     * 
     * This method creates 10 categories with one task each.
     *
     * @return void
     */
    public function run()
    {
        Category::factory()
            ->count(10)
            ->hasTasks(1)
            ->create();
    }
}
