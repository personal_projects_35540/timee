<?php

/**
 * File: database/seeders/TaskSeeder.php
 *
 * Seeder file for Task records.
 *
 * @package   tasks_seeder
 * @category  Seeders
 * @author    Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @copyright 2021 Pastrikos Konstantinos
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version   0.1.0
 * @since     File available since Release 0.0.1
 */

namespace Database\Seeders;

use App\Models\Task;
use Illuminate\Database\Seeder;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * 
     * This method creates 10 tasks with 2 records each.
     *
     * @return void
     */
    public function run()
    {

        Task::factory()
            ->count(10)
            ->hasRecords(2)
            ->create();
    }
}
