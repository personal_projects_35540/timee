<?php

/**
 * File: database/seeders/DatabaseSeeder.php
 *
 * File for executing all database seeders.
 *
 * @package   database_seeder
 * @category  Seeders
 * @author    Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @copyright 2021 Pastrikos Konstantinos
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version   0.1.0
 * @since     File available since Release 0.0.1
 */

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     * 
     * @since 0.1.0 Added calls to custom seeders.
     */
    public function run()
    {

        $this->call(
            [
            UserSeeder::class,
            TaskSeeder::class, // Use this to create categories-tasks-records. 
            //CategorySeeder::class, // Use this to create categories-tasks.
            ]
        );
    }
}
