<?php

/**
 * File: database/factories/TaskFactory.php
 *
 * Factory file for Task Model.
 *
 * @package   task_factory
 * @category  Factories
 * @author    Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @copyright 2021 Pastrikos Konstantinos
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version   0.0.1
 * @since     File available since Release 0.2.0
 */

namespace Database\Factories;

use App\Models\Task;
use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

/** 
 * Factory class for creating Task records.
 * 
 * @category Factories
 * @author   Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @access   public 
 * @version  0.0.1
 */
class TaskFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     * 
     * @since 0.0.1
     */
    protected $model = Task::class;

    /**
     * Define the model's default state.
     * 
     * Because every task must belong to a category we use the CategoryFactory 
     * for generating one category per task.
     * Generated task example:
     * id:          1                        
     * label:       Fuga et enim corporis autem.
     * description: Voluptates soluta eos id ullam. 
     * category_id: 1
     * icon:        😈   
     * color:       #3574bc
     * completed:   0   
     * repetitive   1
     * created_at:  2018-01-17 19:32:55
     * updated_at:  2021-06-19 19:22:05
     * deleted_at:  NULL
     *
     * @return array
     * 
     * @since 0.0.1
     */
    public function definition()
    {
        return [
            'label' => $this->faker->sentence(),
            'description' => $this->faker->sentence(),
            'category_id' => Category::factory(),
            'icon' => $this->faker->emoji(),
            'color' => $this->faker->hexColor(),
            'completed' => $this->faker->boolean(),
            'repetitive' => $this->faker->boolean(),
            'created_at' => $this->faker->dateTime(),
        ];
    }
}
