<?php

/**
 * File: database/factories/CategoryFactory.php
 *
 * Factory file for Category Model.
 *
 * @package   category_factory
 * @category  Factories
 * @author    Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @copyright 2021 Pastrikos Konstantinos
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version   0.0.1
 * @since     File available since Release 0.2.0
 */

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

/** 
 * Factory class for creating Category records.
 * 
 * @category Factories
 * @author   Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @access   public 
 * @version  0.0.1
 */
class CategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     * 
     * @since 0.0.1
     */
    protected $model = Category::class;

    /**
     * Define the model's default state.
     * 
     * Generated category example:
     * id:          1
     * label:       category-ywdo
     * description: Aut sed dicta explicabo quia ducimus sapiente
     * icon:        😜
     * color:       #fabf94
     * created_at:  1997-01-18 04:46:19
     * updated_at:  2021-06-19 19:22:05
     * deleted_at:  NULL
     * 
     * @return array
     * 
     * @since 0.0.1
     */
    public function definition()
    {
        return [
            'label' => $this->faker->lexify('category-????'),
            'description' => $this->faker->sentence(),
            'icon' => $this->faker->emoji(),
            'color' => $this->faker->hexColor(),
            'created_at' => $this->faker->dateTime(),
        ];
    }
}
