<?php

/**
 * File: database/factories/RecordFactory.php
 *
 * Factory file for Record Model.
 *
 * @package   record_factory
 * @category  Factories
 * @author    Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @copyright 2021 Pastrikos Konstantinos
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version   0.0.1
 * @since     File available since Release 0.2.0
 */

namespace Database\Factories;

use App\Models\Record;
use Illuminate\Database\Eloquent\Factories\Factory;

/** 
 * Factory class for creating Record (time sessions) records.
 * 
 * @category Factories
 * @author   Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @access   public 
 * @version  0.0.1
 */
class RecordFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     * 
     * @since 0.0.1
     */
    protected $model = Record::class;

    /**
     * Define the model's default state.
     * 
     * Generated record example:
     * id:          1
     * task_id:     1
     * started_at:  2021-05-29 14:27:09 
     * ended_at:    2021-05-21 21:59:47
     * created_at:  2021-06-25 08:42:45 
     * updated_at:  2021-06-19 19:22:05
     * deleted_at:  NULL    
     *
     * @return array
     * 
     * @since 0.0.1
     */
    public function definition()
    {
        return [
            'started_at' => $this->faker->dateTimeBetween('-1 week', 'now'),
            'ended_at' => $this->faker->dateTimeBetween('-1 week', 'now'),
        ];
    }
}
