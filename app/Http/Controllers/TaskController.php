<?php

/**
 * File: app/Http/Controllers/TaskController.php
 *
 * File containing the resource controller logic for managing tasks.
 *
 * @package   task_resource_controller
 * @task  Controllers
 * @author    Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @copyright 2021 Pastrikos Konstantinos
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version   0.1.0
 * @since     File available since Release 0.3.0
 */

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;

/** 
 * Resource class for Tasks.
 * 
 * This class provides all the necessary methods for managing tasks.
 * A user can create, edit or delete a task.
 * 
 * @task Controllers
 * @author   Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @access   public 
 * @version  0.0.1
 */
class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * 
     * @since 0.0.1
     */
    public function index()
    {
        //return response()->json(Task::all());
        $tasks = Task::with('category')->get();
        return \View::make('app')
            ->with('tasks', $tasks);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * 
     * @since 0.0.1
     */
    public function create()
    {
        return \View::make('app');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * @since 0.0.1
     */
    public function store(Request $request)
    {
        // Data validation.
        // Read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'label' => 'required',
            'category' => 'required',
        );
        $validator = \Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return \Redirect::to('tasks/create')
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            // Proceed with store.
            $task = new Task;
            $task->label        = $request->get('label');
            $task->description  = $request->get('description');
            $task->category_id  = $request->get('category');
            $task->color        = $request->get('color');
            $task->icon         = $request->get('icon');
            $task->repetitive   = $request->get('repetitive') === 'on' ? 1 : 0;
            $task->completed    = $request->get('completed') === 'on' ? 1 : 0;

            try {
                $task->save();
            } catch (\Illuminate\Database\QueryException $e) {
                report($e);
                //dd($e->getMessage());

                return redirect()->route('tasks.index')
                    ->with('error', 'Failed to create task!');
            }


            return redirect()->route('tasks.index')
                ->with('success', 'Successfully created task!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 
     * @since 0.0.1
     */
    public function show($id)
    {
        // Get the task.
        $task = Task::find($id);
        // Show the view and pass the task to it.
        return \View::make('app')->with('task', $task);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 
     * @since 0.0.1
     * @since 0.1.0 Method implementation.
     */
    public function edit($id)
    {
        // Get the task.
        $task = Task::find($id);
        // Show the view and pass the task to it.
        return \View::make('app')->with('task', $task);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 
     * @since 0.0.1
     * @since 0.1.0 Method implementation.
     */
    public function update(Request $request, $id)
    {
        // Validate.
        // Read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'label'       => 'required',
            'category'    => 'required|integer',

        );
        $validator = \Validator::make($request->all(), $rules);

        // process the login
        if ($validator->fails()) {
            return \Redirect::to('tasks/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput($request->all());
        } else {

            $task = Task::find($id);
            $task->label        = $request->get('label');
            $task->description  = $request->get('description');
            $task->category_id  = $request->get('category');
            $task->color        = $request->get('color');
            $task->icon         = $request->get('icon');
            $task->repetitive   = $request->get('repetitive') === 'on' ? 1 : 0;
            $task->completed    = $request->get('completed') === 'on' ? 1 : 0;

            try {
                $task->save();
            } catch (\Illuminate\Database\QueryException $e) {
                //report($e);
                //dd($e->getMessage());
                //dd($e->errorInfo, $e)

                return redirect()->route('tasks.index')
                    ->with('error', 'Failed to update task!');
            }

            return redirect()->route('tasks.index')
                ->with('success', 'Successfully updated task!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 
     * @since 0.0.1
     */
    public function destroy($id)
    {
        $task = Task::find($id);
        $task->delete();

        // Redirect
        \Session::flash('message', 'Task successfully deleted!');
        return \Redirect::to('tasks');
    }
}
