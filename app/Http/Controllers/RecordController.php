<?php

/**
 * File: app/Http/Controllers/RecordController.php
 *
 * File containing the resource controller logic for managing records.
 *
 * @package   record_resource_controller
 * @category  Controllers
 * @author    Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @copyright 2021 Pastrikos Konstantinos
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version   0.0.1
 * @since     File available since Release 0.3.0
 */

namespace App\Http\Controllers;

use App\Models\Record;
use Illuminate\Http\Request;

/** 
 * Resource class for Records.
 * 
 * This class provides all the necessary methods for managing records.
 * 
 * @category Controllers
 * @author   Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @access   public 
 * @version  0.0.1
 */
class RecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * 
     * @since 0.0.1
     */
    public function index()
    {
        return response()->json(Record::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * 
     * @since 0.0.1
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * @since 0.0.1
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 
     * @since 0.0.1
     */
    public function show($id)
    {
        $record = Record::find($id);
        return response()->json($record);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 
     * @since 0.0.1
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 
     * @since 0.0.1
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 
     * @since 0.0.1
     */
    public function destroy($id)
    {
        $record = Record::find($id);
        $record->delete();
    }
}
