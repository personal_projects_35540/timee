<?php

/**
 * File: app/Http/Controllers/CategoryController.php
 *
 * File containing the resource controller logic for managing categories.
 *
 * @package   category_resource_controller
 * @category  Controllers
 * @author    Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @copyright 2021 Pastrikos Konstantinos
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version   0.2.0
 * @since     File available since Release 0.3.0
 */

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

/**
 * Resource class for Categories.
 * 
 * This class provides all the necessary methods for managing categories.
 * A user can create, edit or delete a category.
 * 
 * @category Controllers
 * @author   Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @access   public 
 * @version  0.0.1
 */
class CategoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * 
     * @since 0.0.1
     */
    public function index()
    {
        //return response()->json(Category::all());

        return \View::make('app')->with('categories', Category::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * 
     * @since 0.0.1
     * @since 0.2.0 Method implementation.
     */
    public function create()
    {
        return \View::make('app');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * 
     * @since 0.0.1
     * @since 0.2.0 Method implementation.
     */
    public function store(Request $request)
    {
        // Data validation.
        // Read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'label' => 'required',
        );
        $validator = \Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return \Redirect::to('categories/create')
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            // Proceed with store.
            $category = new Category;
            $category->label        = $request->get('label');
            $category->description  = $request->get('description');
            $category->color        = $request->get('color') ?? '#0d6efd';
            $category->icon         = $request->get('icon') ?? 'tag';

            try {
                $category->save();
            } catch (\Illuminate\Database\QueryException $e) {
                report($e);
                //dd($e->getMessage());

                return redirect()->route('categories.index')
                    ->with('error', 'Failed to create category!');
            }


            return redirect()->route('categories.index')
                ->with('success', 'Successfully created category!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * 
     * @since 0.0.1
     */
    public function show($id)
    {
        // Get the category.
        $category = Category::find($id);
        //return response()->json($category);

        // Show the view and pass the category to it.
        return \View::make('app')->with('category', $category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * 
     * @since 0.0.1
     * @since 0.2.0 Method implementation.
     */
    public function edit($id)
    {
        // Get the category.
        $category = Category::find($id);
        //return response()->json($category);

        // Show the view and pass the category to it.
        return \View::make('app')->with('category', $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     * 
     * @since 0.0.1
     */
    public function update(Request $request, $id)
    {

        // Validate.
        // Read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'label'       => 'required',
            'color'       => 'required',

        );
        $validator = \Validator::make($request->all(), $rules);

        // process the login
        if ($validator->fails()) {
            return \Redirect::to('categories/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput($request->all());
        } else {


            $category = Category::find($id);
            $category->label       = $request->get('label');
            $category->description = $request->get('description');
            $category->color       = $request->get('color') ?? '#0d6efd';
            $category->icon        = $request->get('icon') ?? 'tag';

            try {
                $category->save();
            } catch (\Illuminate\Database\QueryException $e) {
                report($e);
                dd($e->getMessage());

                return redirect()->route('categories.index')
                    ->with('error', 'Failed to update category!');
            }

            return redirect()->route('categories.index')
                ->with('success', 'Successfully updated category!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * 
     * @since 0.0.1
     */
    public function destroy($id)
    {

        $category = Category::find($id);
        $category->delete();

        // Redirect
        \Session::flash('message', 'Category successfully deleted!');
        return \Redirect::to('categories');
    }
}
