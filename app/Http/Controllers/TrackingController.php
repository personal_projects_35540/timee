<?php

/**
 * File: app/Http/Controllers/TrackingController.php
 *
 * File containing the resource controller logic for managing tasks.
 *
 * @package   task_resource_controller
 * @category  Controllers
 * @author    Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @copyright 2021 Pastrikos Konstantinos
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version   0.1.0
 * @since     File available since Release 0.4.0
 */

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;

use function PHPSTORM_META\map;

/** 
 * Resource class for Tasks.
 * 
 * This class provides all the necessary methods for managing tasks.
 * A user can create, edit or delete a task.
 * 
 * @category Controllers
 * @author   Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @access   public 
 * @version  0.0.1
 */
class TrackingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * 
     * @since 0.0.1
     */
    public function index()
    {

        $tasks = $this->setEmptyCategoryLabel(Task::with('category')->get());

        return \View::make('app')->with('tasks', $tasks);
    }


    /**
     * Find and replace empty categories label.
     * 
     * Because the user is able to delete categories we must handle the case when
     * the collection includes tasks with deleted category. In this scenario the 
     * relations object is null so we have to create an new object and set 
     * the appropriate label.
     * 
     * Example of Task model with empty category:
     * #relations: array:1 ["category" => null]
     * 
     * @param Illuminate\Database\Eloquent\Collection $tasks Task collection.
     * 
     * @return Illuminate\Database\Eloquent\Collection
     * 
     * @since 0.1.0
     */
    public function setEmptyCategoryLabel(\Illuminate\Database\Eloquent\Collection $tasks)
    {
        // Find and replace empty categories label.
        $tasks->map(function ($task) {

            if ($task->category === null) {
                $task->category = new \stdClass();
                $task->category->label = 'uncategorized';
                return $task;
            }
        });

        return $tasks;
    }
}
