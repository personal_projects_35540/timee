<?php

/**
 * File: app/Models/Category.php
 *
 * File containing the logic of Category Model.
 *
 * @package   category_model
 * @category  Models
 * @author    Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @copyright 2021 Pastrikos Konstantinos
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version   0.1.0
 * @since     File available since Release 0.2.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/** 
 * Category Model Class.
 * 
 * @category Models
 * @author   Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @access   public 
 * @version  0.1.0
 */
class Category extends Model
{
    use HasFactory;

    /**
     * SoftDeletes Trait.
     * 
     * @since 0.1.0
     */
    use SoftDeletes;

    /**
     * Get the tasks for the category.
     * 
     * @since 0.0.1
     */
    public function tasks()
    {
        return $this->hasMany(Task::class);
    }
}
