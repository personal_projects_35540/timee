<?php

/**
 * File: app/Models/Record.php
 *
 * File containing the logic of Record Model.
 *
 * @package   record_model
 * @category  Models
 * @author    Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @copyright 2021 Pastrikos Konstantinos
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version   0.0.1
 * @since     File available since Release 0.2.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/** 
 * Record Model Class.
 * 
 * @category Models
 * @author   Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @access   public 
 */
class Record extends Model
{
    use HasFactory;

    /**
     * Get the task that the record belongs to.
     * 
     * One record must belong to one task.
     * 
     * @since 0.0.1
     */
    public function task()
    {
        return $this->belongsTo(Task::class);
    }
}
