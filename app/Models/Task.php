<?php

/**
 * File: app/Models/Task.php
 *
 * File containing the logic of Task Model.
 *
 * @package   task_model
 * @category  Models
 * @author    Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @copyright 2021 Pastrikos Konstantinos
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version   0.1.0
 * @since     File available since Release 0.2.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/** 
 * Task Model Class.
 * 
 * @category Models
 * @author   Pastrikos Konstantinos <pastrikos.ko@gmail.com>
 * @access   public 
 * @version  0.1.0
 */
class Task extends Model
{
    use HasFactory;

    /**
     * SoftDeletes Trait.
     * 
     * @since 0.1.0
     */
    use SoftDeletes;

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'completed' => false,
        'repetitive' => false,
        'color' => '#6b848a',
        'icon' => 'tasks'
    ];

    /**
     * Get the records for the task.
     * 
     * One task could have multiple records.
     * 
     * @since 0.0.1
     */
    public function records()
    {
        return $this->hasMany(Record::class);
    }

    /**
     * Get the category that the task belongs to.
     * 
     * One task must belong to one category.
     * 
     * @since 0.0.1
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
